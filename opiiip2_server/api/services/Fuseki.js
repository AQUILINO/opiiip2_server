/****************************************************************************
* Servicio : que centraliza las solicitudes de datos al servidor de data		*
*						 semantica fuseki  																							*
* referencia : https://www.npmjs.com/package/nodemailer 				           	*
*****************************************************************************/

module.exports = {


	query : function(consulta , cb){

		// libreria que permite realizar solicitudes http de forma comoda :3
		var request = require('request')

		// usaremos la libreria para traernos las configuraciones necesarias para
		// realizar request de tipo sparql
		var sparql = require('sparql-http-client')

		// configuramos el request de modo que pueda realizar solicitudes de tipo sparql
		sparql.request = sparql.requestModuleRequest(request)

		// creamos la instancia sparql a la que realizaremos la solicitud
		var servicio = new sparql({endpointUrl: 'http://localhost:3030/ds/query'})

		// extraemos el query
		var query = consulta.query;

		servicio.selectQuery(query, function (error, response) {
			if (error) {
				console.log('Error al conectar con el servicio de coneccion con fuseki'.red);
				return cb('Error al conectar con Fuseki');
			};

		  var respuesta = response.body;

  		respuesta = respuesta.replace(/http:\/\/www\.w3\.org\/2002\/07\/owl#/g,'owl:')
		  .replace(/http:\/\/www\.w3\.org\/1999\/02\/22-rdf-syntax-ns#/g,'rdf:')
		  .replace(/http:\/\/www\.w3\.org\/2000\/01\/rdf-schema#/g,'rdfs:')
		  .replace(/http:\/\/www\.w3\.org\/2001\/XMLSchema#/g,'xds:')
		  .replace(/http:\/\/www\.co-ode\.org\/ontologies\/pizza\/pizza\.owl#/g,'pizza:');
		  respuesta = JSON.parse(respuesta);

		  return cb(undefined, respuesta);
		})
	},

	update : function(cambios , cb){

		// libreria que permite realizar solicitudes http de forma comoda :3
		var request = require('request')

		// usaremos la libreria para traernos las configuraciones necesarias para
		// realizar request de tipo sparql
		var sparql = require('sparql-http-client')

		// configuramos el request de modo que pueda realizar solicitudes de tipo sparql
		spaqrl.request = spaqrl.requestModuleRequest(request)

		// creamos la instancia sparql a la que realizaremos la solicitud
		var servicio = new SparqlHttp({endpointUrl: 'http://localhost:3030/ds/update'})

		// extraemos el query
		var query = cambios.query;

		servicio.updateQuery(query, function (error, response) {
			if (error) {
				console.log('Error al conectar con el servicio de coneccion con fuseki'.red);
				return cb('Error al conectar con Fuseki');
			};

		  var respuesta = JSON.stringify(response.body);

  		respuesta = respuesta.replace(/http:\/\/www\.w3\.org\/2002\/07\/owl#/g,'owl:')
		  .replace(/http:\/\/www\.w3\.org\/1999\/02\/22-rdf-syntax-ns#/g,'rdf:')
		  .replace(/http:\/\/www\.w3\.org\/2000\/01\/rdf-schema#/g,'rdfs:')
		  .replace(/http:\/\/www\.w3\.org\/2001\/XMLSchema#/g,'xds:')
		  .replace(/http:\/\/www\.co-ode\.org\/ontologies\/pizza\/pizza\.owl#/g,'pizza:');
		  respuesta = JSON.parse(respuesta);

		  return cb(undefined, respuesta);
		})
	},

	// prefijos
	prefijos : {
		owl : 'prefix owl: <http://www.w3.org/2002/07/owl#>',
		rdf : 'prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>',
		rdfs : 'prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>',
		xds : 'prefix xsd: <http://www.w3.org/2001/XMLSchema#>',
		pizza : 'prefix pizza: <http://www.co-ode.org/ontologies/pizza/pizza.owl#>',
	}
}
