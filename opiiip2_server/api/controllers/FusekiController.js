/**
 * FusekiController
 *
 * @description :: controlador de acceso al web service de querys al servidor jena fuseki
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


	/*
	*		accion :: consulta_1
	*		descripcion :: realiza las operaciones necesarias para realizar una consulta 1
	*									 en la base de datos semantica sobre pizzas 
	*		consulta :: consulta los ingredientes de cada pizza
	*/

	ingredientes : function(req, res){
		
		var prefijos = Fuseki.prefijos.owl +
									' ' + Fuseki.prefijos.xds + 
									' ' + Fuseki.prefijos.rdf + 
									' ' + Fuseki.prefijos.rdfs +
									' ' + Fuseki.prefijos.pizza;

		var query = prefijos +  'SELECT ?pizza ?topping ' + 
														'WHERE { ' +
    													'?pizza rdfs:subClassOf pizza:NamedPizza .' +
    													'?pizza rdfs:subClassOf ?restriccion .' +
    													'?restriccion owl:onProperty pizza:hasTopping .' +
    													'?restriccion owl:someValuesFrom ?topping .' +
														'}';

		Fuseki.query({
			query: query
		},function(err, resultado){
			
			if (err) {
				return res.json({error : err});
			};

			if (req.wantsJSON) {
		    return res.jsonx(resultado);
		  }

		  res.locals({resultado : resultado});
			return res.view('consultas/consulta');
		});
	
	},

	/*
	*		accion :: pizzas
	*		descripcion :: realiza las operaciones necesarias para realizar una consulta 2
	*									 en la base de datos semantica sobre pizzas 
	*		consulta :: todas las pizzas registradas
	*/

	pizzas : function(req, res){
		var prefijos = Fuseki.prefijos.owl +
							' ' + Fuseki.prefijos.xds + 
							' ' + Fuseki.prefijos.rdf + 
							' ' + Fuseki.prefijos.rdfs +
							' ' + Fuseki.prefijos.pizza;

		var query = prefijos +  'SELECT ?pizzas ' + 
														'WHERE { ' +
    													'?pizzas rdfs:subClassOf pizza:NamedPizza ' +
														'}';

		Fuseki.query({
			query: query
		},function(err, resultado){
			
			if (err) {
				return res.json({error : err});
			};

			if (req.wantsJSON) {
		    return res.jsonx(resultado);
		  }

		  res.locals({resultado : resultado});
			return res.view('consultas/consulta');
		});
	
	},

	/*
	*		accion :: toppings
	*		descripcion :: realiza las operaciones necesarias para realizar una consulta 3
	*									 en la base de datos semantica sobre pizzas 
	*		consulta : todos los toppings registrados
	*/

	toppings : function(req, res){
		
		var prefijos = Fuseki.prefijos.owl +
							' ' + Fuseki.prefijos.xds + 
							' ' + Fuseki.prefijos.rdf + 
							' ' + Fuseki.prefijos.rdfs +
							' ' + Fuseki.prefijos.pizza;

		var query = prefijos +  'SELECT ?superTopping ?topping  ' + 
														'WHERE { ' +
    													'?superTopping rdfs:subClassOf pizza:PizzaTopping .' +
    													'?topping rdfs:subClassOf ?superTopping .' +
														'}';

		Fuseki.query({
			query: query
		},function(err, resultado){
			
			if (err) {
				return res.json({error : err});
			};

			if (req.wantsJSON) {
		    return res.jsonx(resultado);
		  }

		  res.locals({resultado : resultado});
			return res.view('consultas/consulta');
		});
	
	},

};

